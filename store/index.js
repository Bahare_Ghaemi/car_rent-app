import axios from 'axios'

// for sort method
export const strict = false

export const state = () => ({
  //alert
  errorAlertStatus: false,

  // loading
  loadingIsActive: false,

  // login
  loggedInUser: typeof window !== 'undefined' ? localStorage.loggedInUser : {},

  // cars
  cars: [],

  popularCars: [],

  // checkbox options
  checkboxs: [{
      title: 'TYPE',
      options: ['Sport', 'SUV', 'MPV', 'Sedan', 'Coupe', 'Hatchback'],
      visibility: false,
    },
    {
      title: 'CAPACITY',
      options: [2, 4, 6, 8],
      visibility: false,
    },
    {
      title: 'PRICE',
      option: [],
      visibility: false,
    }
  ],

  // the car that you're watching it in browser
  currentCar: {},

  // count of reviewers of each car
  reviewersCount: 0,
})

export const getters = {
}

console.log(typeof window !== 'undefined' ? localStorage.loggedInUser : 'not local');

export const mutations = {
  // alert
  CHANGE_ALERT_STATUS(state, status) {
    state.errorAlertStatus = status
  },

  // login
  SET_LOGGEDIN_USER(state, user) {
    state.loggedInUser = user
  },

  // loading
  CHANGE_LOADING_STATUS(state, status) {
    state.loadingIsActive = status
  },

  // set cars
  SET_CARS(state, data) {
    console.log('setting cars');
    state.cars = data
  },

  // set current car mutation
  SET_CURRENT_CAR(state, currCar) {
    state.currentCar = currCar
  },

  // set count of reviewers of each car
  // SET_REVIEWRS_COUNT(state,name){
  //   state.currentCar.forEach(element => {
      
  //   });
  // }
}

export const actions = {
  // login
  setLoggedInUser(context, user) {
    context.commit('SET_LOGGEDIN_USER', user)
  },

  //loading
  changeLoadingStatus(context, status) {
    context.commit('CHANGE_LOADING_STATUS', status)
  },

  // set cars
  async setCars(context) {
    // context.commit('SET_CARS', data)
    await axios.get('http://localhost:3001/cars').then((response) => {
      console.log(response);
      context.commit('SET_CARS', response.data)
    })
  },

  // get current car
  async setCurrentCar(context, name) {
    await axios.get('http://localhost:3001/cars').then((response) => {
      response.data.forEach(car => {
        if (name === car.name) {
          context.commit('SET_CURRENT_CAR', car)
        }
      });
      // const currCar = response.data.filter((car) => {
      //   return car.name = name
      // })
      // context.commit('SET_CURRENT_CAR', currCar)
    })
  }
}
