/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./components/**/*.{js,vue,ts}",
    "./layouts/**/*.vue",
    "./pages/**/*.vue",
    "./plugins/**/*.{js,ts}",
    "./nuxt.config.{js,ts}",
  ],
  theme: {
    extend: {},
    screens: {
      'xxs': '400px',
      'xs': '560px',
      'sm': '640px',
      'md': '768px',
      '2md': '900px',
      'lg': '1024px',
      '2lg': '1150px',
      'xl': '1280px',
      '2xl': '1536px',
    },
  },
  plugins: [],
}
